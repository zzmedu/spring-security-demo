# spring-security-demo

#### 介绍
使用springboot 2.3.6 + spring security做权限认证，redis存储token；通过存储在redis中的token可做单体认证也可做集群认证，也可做单点登录等等；全程使用json数据交互；天生支持前后端分离，也可不分离；反正这个就是有点点牛逼哦;目前已经实现了动态数据库登录认证和接口权限认证

#### 软件架构
spring boot 2.3.6
spring security
redis
jwt
mysql


#### 安装教程

1.  下载：https://gitee.com/zzmedu/spring-security-demo.git
2.  导入数据库表：表在/db/spring-security-demo.sql

#### 使用说明

1.  在application修改数据库密码和redis连接信息
2.  运行SpringSecurityDemoApplication.java启动类
3.  使用postman测试登录接口：http://localhost:8099/securityDemo/login    请求方法：post    请求
4.  json格式：{
      "username":"zzm",
      "password":"zzm123456" 
    }
5.  登录之后，可测试hello接口；完整路径：https://localhost:8099/securityDemo/hello

#### 其他

1.详细集成过程可看我的博客；博客地址：http://it.zzmblog.top
