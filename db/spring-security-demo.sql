/*
 Navicat Premium Data Transfer

 Source Server         : mysql5.7
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : spring-security-demo

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 19/11/2020 17:30:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `name_zh` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称【中文】',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口',
  `parent_id` bigint(20) NOT NULL COMMENT '父级编号',
  `level` bigint(20) NOT NULL COMMENT '菜单级别',
  `is_menu` tinyint(1) NOT NULL COMMENT '是否是菜单',
  `icon` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'layui-icon-app' COMMENT '菜单图标',
  `label` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限标签',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统ALL -> 菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 'home page', '首页', '/', 0, 1, 1, 'layui-icon-home', '');
INSERT INTO `sys_menu` VALUES (2, 'admin home page', '分析页', '/console/admin', 1, 2, 1, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (15, 'sys', '系统设置', '/', 0, 15, 1, 'layui-icon-set', '');
INSERT INTO `sys_menu` VALUES (16, 'system user', '账号管理', '/sysuser/SysUser', 15, 16, 1, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (17, 'system role', '角色管理', '/sysrole/SysRole', 15, 17, 1, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (18, 'system menu', '菜单管理', '/sysmenu/SysMenu', 15, 18, 1, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (26, 'find by page', '分页', '/sysMenu/findByPage', 18, 19, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (27, 'find by page', '分页', '/sysRole/findByPage', 17, 20, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (28, 'find by page', '分页', '/sysUser/findByPage', 16, 21, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (32, 'add', '新增', '/sysMenu/add', 18, 25, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (33, 'add', '新增', '/sysUser/add', 16, 26, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (34, 'add', '新增', '/sysRole/add', 17, 27, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (35, 'add', '新增', '/payRate/add', 5, 28, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (37, 'oauth', '授权', '/sysRole/oauth', 17, 30, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (38, 'delete', '删除', '/sysRole/delete/**', 17, 31, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (39, 'update', '修改', '/sysRole/update', 17, 35, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (40, 'update', '修改', '/sysMenu/update', 18, 33, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (41, 'delete', '删除', '/sysMenu/delete', 18, 34, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (42, 'select', '下拉查询', '/sysRole/selectList', 17, 35, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (58, 'left menu', '左侧菜单', '/sysMenu/leftSideMenu/**', 0, 39, 2, 'layui-icon-app', '');
INSERT INTO `sys_menu` VALUES (60, 'user home page', '分析页', '/console/user', 1, 1, 1, 'layui-icon-app', '运营');
INSERT INTO `sys_menu` VALUES (64, 'update', '修改', '/sysUser/update', 16, 26, 0, 'lay-app-update', '');
INSERT INTO `sys_menu` VALUES (65, 'lb home page', '分析页【鲁班小店】', '/console/lb', 1, 23, 1, 'lay-app-home', '鲁班小店');
INSERT INTO `sys_menu` VALUES (86, 'plain', '分析页', '/console/user', 1, 36, 1, 'lay-app', '普通');
INSERT INTO `sys_menu` VALUES (91, 'api info', '接口管理', '/', 0, 12, 1, 'layui-icon-key', '');
INSERT INTO `sys_menu` VALUES (92, 'api page', 'api页面路口', '/interface-ui', 91, 45, 0, 'lay-app', '');
INSERT INTO `sys_menu` VALUES (93, 'api dev', '接口开发', '/apiinfo/ApiInfo', 91, 56, 1, 'lay-app', '');
INSERT INTO `sys_menu` VALUES (94, 'ad', '广告管理', '/', 0, 15, 1, 'layui-icon-tree', '');
INSERT INTO `sys_menu` VALUES (95, 'ad list', '广告列表', '/ad/AdList', 94, 16, 1, 'layui-icon-tree', '');
INSERT INTO `sys_menu` VALUES (96, 'ad list page', '分页', '/interface-ui/ad/findByPage', 95, 17, 0, 'layui-icon-tree', '');
INSERT INTO `sys_menu` VALUES (97, 'hello', 'hello', '/hello', 56, 12, 0, 'layui-icon-app', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `name_zh` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称【中文】',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '状态；1：正常;2：禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'ROLE_admin', '超级管理员', 1);
INSERT INTO `sys_role` VALUES (2, 'ROLE_user', '普通成员', 1);
INSERT INTO `sys_role` VALUES (3, 'ROLE_lb', '鲁班小店管理员', 1);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(11) NOT NULL COMMENT '角色编号',
  `menu_id` bigint(11) NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1578 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统ALL -> 角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1365, 2, 1);
INSERT INTO `sys_role_menu` VALUES (1366, 2, 86);
INSERT INTO `sys_role_menu` VALUES (1367, 2, 58);
INSERT INTO `sys_role_menu` VALUES (1551, 1, 1);
INSERT INTO `sys_role_menu` VALUES (1552, 1, 2);
INSERT INTO `sys_role_menu` VALUES (1553, 1, 15);
INSERT INTO `sys_role_menu` VALUES (1554, 1, 16);
INSERT INTO `sys_role_menu` VALUES (1555, 1, 28);
INSERT INTO `sys_role_menu` VALUES (1556, 1, 33);
INSERT INTO `sys_role_menu` VALUES (1557, 1, 64);
INSERT INTO `sys_role_menu` VALUES (1558, 1, 17);
INSERT INTO `sys_role_menu` VALUES (1559, 1, 27);
INSERT INTO `sys_role_menu` VALUES (1560, 1, 34);
INSERT INTO `sys_role_menu` VALUES (1561, 1, 37);
INSERT INTO `sys_role_menu` VALUES (1562, 1, 38);
INSERT INTO `sys_role_menu` VALUES (1563, 1, 39);
INSERT INTO `sys_role_menu` VALUES (1564, 1, 42);
INSERT INTO `sys_role_menu` VALUES (1565, 1, 18);
INSERT INTO `sys_role_menu` VALUES (1566, 1, 26);
INSERT INTO `sys_role_menu` VALUES (1567, 1, 32);
INSERT INTO `sys_role_menu` VALUES (1568, 1, 40);
INSERT INTO `sys_role_menu` VALUES (1569, 1, 41);
INSERT INTO `sys_role_menu` VALUES (1570, 1, 58);
INSERT INTO `sys_role_menu` VALUES (1571, 1, 91);
INSERT INTO `sys_role_menu` VALUES (1572, 1, 92);
INSERT INTO `sys_role_menu` VALUES (1573, 1, 93);
INSERT INTO `sys_role_menu` VALUES (1574, 1, 94);
INSERT INTO `sys_role_menu` VALUES (1575, 1, 95);
INSERT INTO `sys_role_menu` VALUES (1576, 1, 96);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account_non_expired` tinyint(1) NOT NULL DEFAULT 1 COMMENT '用户的状态',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '账户是否可用',
  `lang` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'zh-CN' COMMENT '语言',
  `nik_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$vW6/7WQWZA2wZYTVw0Ac1.KmY0olEh14lYu5YMglvC9NQrzfoPhQ2', 0, 1, 'zh-CN', '超级管理员');
INSERT INTO `sys_user` VALUES (2, 'zzm', '$2a$10$q8VyYsHkorVs4ba0vxT44uAAAum/EwoauSvv7LWJDY2QRMx8dQxfi', 1, 1, 'zh-CN', '走芝麻');
INSERT INTO `sys_user` VALUES (3, 'frank', '$2a$10$YQeCHrpyGCINel.z1g8vF.Hx09fTcuQYf8NbmrpSK8ckP1TfT1y2y', 1, 1, 'zh-CN', 'frank');
INSERT INTO `sys_user` VALUES (4, 'zyf', '$2a$10$hWnlw6Ji4PNqK0lab7tz/eVJJr3dm8FKhfvmIu9RYY2B2k2s/Rx/O', 1, 1, 'zh-CN', '张永峰');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(11) NOT NULL COMMENT '账户编号',
  `role_id` bigint(11) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2, 1);
INSERT INTO `sys_user_role` VALUES (3, 3, 1);
INSERT INTO `sys_user_role` VALUES (4, 4, 2);

SET FOREIGN_KEY_CHECKS = 1;
