package com.huntmobi.springsecuritydemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huntmobi.springsecuritydemo.base.pojo.SysUser;
import com.huntmobi.springsecuritydemo.base.dto.sysuser.FindByPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
    IPage<FindByPage> findByPage(Page<SysUser> page);
    SysUser findUserByUsername(@Param("userName") String userName);
}
