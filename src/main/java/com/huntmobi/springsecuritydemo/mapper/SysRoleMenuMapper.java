package com.huntmobi.springsecuritydemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huntmobi.springsecuritydemo.base.pojo.SysRoleMenu;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
