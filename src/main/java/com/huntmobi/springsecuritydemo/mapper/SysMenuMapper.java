package com.huntmobi.springsecuritydemo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huntmobi.springsecuritydemo.base.pojo.SysMenu;
import com.huntmobi.springsecuritydemo.base.dto.sysmenu.FindByPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<FindByPage> findByPage(@Param("id") Long id);

    List<SysMenu> findMenuByUserName(@Param("roleName") String roleName);

    List<FindByPage> leftSideMenu(@Param("roleName") String roleName);
}
