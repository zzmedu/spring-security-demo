package com.huntmobi.springsecuritydemo.base.util;


import com.huntmobi.springsecuritydemo.base.pojo.SysMenu;
import com.huntmobi.springsecuritydemo.base.dto.sysmenu.FindByPage;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 11:02 2020/8/12
 * </P>
 **/
public class TreeUtils {

    /**
     * 生成菜单
     *
     * @param sysMenus
     *@return
     */
    public static List getMenu(List<FindByPage> sysMenus) {
        //实例化一个根节点数组
        List<FindByPage> root = new ArrayList<>();
        for (FindByPage sysMenu : sysMenus) {
            //数据库查出pid为0或空，就直接加入到根节点
            if (sysMenu.getParentId() == 0) {
                root.add(sysMenu);
            }
        }
        for (FindByPage sysMenu : root) {
            sysMenu.setChildren(getChildren(sysMenus, sysMenu));
        }
        return root;
    }

    /**
     * 生成树枝
     *
     * @param sysMenus  生成菜单传过来的剩余数据
     * @param root 根节点对象
     * @return
     */
    public static List<FindByPage> getChildren(List<FindByPage> sysMenus, SysMenu root) {
        List<FindByPage> children = new ArrayList<>();
        for (FindByPage sysMenu : sysMenus) {
            if (sysMenu.getParentId() == root.getId()) {
                children.add(sysMenu);
            }
        }
        for (FindByPage child : children) {
            child.setChildren(getChildren(sysMenus, child));
        }
        return children;
    }
}
