package com.huntmobi.springsecuritydemo.base.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.huntmobi.springsecuritydemo.base.dto.sysmenu.FindByPage;
import com.huntmobi.springsecuritydemo.page.PageForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenu extends PageForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String name;

    private String nameZh;

    private String path;

    private String icon;

    private String label;

    private Long parentId;

    private Long level;

    private Boolean isMenu;

    @TableField(exist = false)
    private List<FindByPage> children;

    @TableField(exist = false)
    private Boolean checked;
}
