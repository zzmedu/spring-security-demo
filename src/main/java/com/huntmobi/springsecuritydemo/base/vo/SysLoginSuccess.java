package com.huntmobi.springsecuritydemo.base.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;

/**
 * @BelongsProject: spring-security-demo
 * @BelongPackage: com.huntmobi.springsecuritydemo.base.vo
 * @Author: zouzhimin
 * @Date: 2020/11/19 12:03
 * @Description: 登录成功后需要返回的字段
 **/
@Setter
@Getter
public class SysLoginSuccess {
    private String token;
    private String nikName;
    private List<HashMap<String,String>> roles;
}
