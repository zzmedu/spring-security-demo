package com.huntmobi.springsecuritydemo.base.dto.sysrole;


import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 9:35 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class FindByPage extends com.huntmobi.springsecuritydemo.page.PageForm {
}
