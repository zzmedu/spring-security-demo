package com.huntmobi.springsecuritydemo.base.dto.sysuser;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 9:35 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class Add {

    @NotBlank(message = "账号不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotNull(message = "请选择角色")
    private Long roleId;

}
