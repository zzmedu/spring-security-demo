package com.huntmobi.springsecuritydemo.base.dto.sysmenu;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 9:44 2020/8/13
 * </P>
 **/
@Setter
@Getter
public class Add {

    @NotBlank(message = "菜单名称【英文】不能为空")
    private String name;

    @NotBlank(message = "菜单名称【中文】不能为空")
    private String nameZh;

    @NotBlank(message = "接口名称不能为空")
    private String path;

    private String icon;

    private String label;

    @NotNull(message = "权限级别不能为空")
    private Long level;

    @NotNull(message = "父级菜单编号不能为空")
    private Long parentId;

    @NotNull(message = "是否为菜单不能为空")
    private Boolean isMenu;
}
