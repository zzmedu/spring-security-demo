package com.huntmobi.springsecuritydemo.base.dto.sysrole;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 16:12 2020/8/12
 * </P>
 **/
@Setter
@Getter
public class Oauth {

    @NotNull(message = "角色编号不为空")
    private Long id;

    private List<Long> menuIds;
}
