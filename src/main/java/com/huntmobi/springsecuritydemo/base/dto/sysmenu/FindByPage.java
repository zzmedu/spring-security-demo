package com.huntmobi.springsecuritydemo.base.dto.sysmenu;

import com.huntmobi.springsecuritydemo.base.pojo.SysMenu;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 10:41 2020/8/12
 * </P>
 **/
@Setter
@Getter
public class FindByPage extends SysMenu {

    private Long id;

}
