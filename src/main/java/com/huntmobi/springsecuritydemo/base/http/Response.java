package com.huntmobi.springsecuritydemo.base.http;



import com.huntmobi.springsecuritydemo.base.enums.HttpEnum;

import java.io.Serializable;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 11:16 2019/4/24
 * </P>
 **/
public final class Response implements Serializable {

    private static final long serialVersionUID = 1725159680599612404L;

    public static Request success() {
        Request request = new Request();
        request.setCode(HttpEnum.SUCCESS.getCode());
        request.setMsg(HttpEnum.SUCCESS.getMsg());
        return request;
    }

    public static Request success(Object object) {
        Request request = new Request();
        request.setCode(HttpEnum.SUCCESS.getCode());
        request.setMsg(HttpEnum.SUCCESS.getMsg());
        request.setData(object);
        return request;
    }

    public static Request success(int code,String msg, Object object) {
        Request request = new Request();
        request.setCode(code);
        request.setMsg(msg);
        request.setData(object);
        return request;
    }

    public static Request error() {
        Request request = new Request();
        request.setCode(HttpEnum.fail.getCode());
        request.setMsg(HttpEnum.fail.getMsg());
        return request;
    }

    public static Request error(int code,String msg) {
        Request request = new Request();
        request.setCode(code);
        request.setMsg(msg);
        return request;
    }
}
