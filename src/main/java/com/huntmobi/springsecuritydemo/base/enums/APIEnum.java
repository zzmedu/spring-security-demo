package com.huntmobi.springsecuritydemo.base.enums;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:43 2020/7/9
 * </P>
 **/
public enum APIEnum {

    VERCODE("/vercode"),

    LOGIN("/login"),

    LOGOUT("/logout"),

    FAVICONICO("/favicon.ico"),

    DOC("/doc.html"),

    WEBJARS("/webjars/**"),

    SWAGGERRESOURCES("/swagger-resources/**"),

    v2ApiDocs("/v2/api-docs"),

    v2ApiDocsExt("/v2/api-docs-ext"),

    TEEN3PATTI("http://api.teen3patti.com:8084/ajax/"),

    ORDERLIST("https://openapi-fxg.jinritemai.com"),

    SFWEBSITEDEV("https://sfapi-sbox.sf-express.com/std/service"),

    SFWEBSITEPRO("https://sfapi.sf-express.com/std/service"),

    REMEMBERME("remember-me"),

    LEFTMENU("/sysMenu/leftSideMenu");

    private String api;

    APIEnum(String api){
        this.api = api;
    }

    public String getApi(){
        return api;
    }
}
