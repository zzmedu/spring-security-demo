package com.huntmobi.springsecuritydemo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huntmobi.springsecuritydemo.base.enums.HttpEnum;
import com.huntmobi.springsecuritydemo.base.http.Request;
import com.huntmobi.springsecuritydemo.base.http.Response;
import com.huntmobi.springsecuritydemo.base.pojo.SysUser;
import com.huntmobi.springsecuritydemo.base.pojo.SysUserRole;
import com.huntmobi.springsecuritydemo.base.util.SecurityUtil;
import com.huntmobi.springsecuritydemo.base.dto.sysuser.Add;
import com.huntmobi.springsecuritydemo.base.dto.sysuser.FindByPage;
import com.huntmobi.springsecuritydemo.base.dto.sysuser.Update;
import com.huntmobi.springsecuritydemo.service.ISysUserRoleService;
import com.huntmobi.springsecuritydemo.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    private ISysUserRoleService iSysUserRoleService;

    @PostMapping("/findByPage")
    public Request findByPage(@Valid @RequestBody FindByPage findByPage){

        //这里进行了模糊查询+分页查询条件
        IPage<FindByPage> sysUserIPage = iSysUserService.findByPage(new Page<>(findByPage.getPage(),findByPage.getLimit()));

        return Response.success(sysUserIPage);
    }

    @PostMapping("/add")
    public Request add(@Valid @RequestBody Add add){
        //判断是否存在账号
        if(iSysUserService.getOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getUsername,add.getUsername())) != null){
            return Response.error(HttpEnum.ISEXIS.getCode(), HttpEnum.ISEXIS.getMsg());
        }
        //密码加密
        add.setPassword(SecurityUtil.getPassword(add.getPassword()));

        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(add,sysUser);

        if(iSysUserService.save(sysUser)){
            //开始新增角色联名表数据
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(iSysUserService.getOne(new QueryWrapper<SysUser>().lambda().select(SysUser::getId).eq(SysUser::getUsername,add.getUsername())).getId());
            sysUserRole.setRoleId(add.getRoleId());
            if(iSysUserRoleService.save(sysUserRole)){
                return Response.success();
            }

        }

        return Response.error();
    }

    @PostMapping("/update")
    public Request update(@Valid @RequestBody Update update){

        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(update,sysUser);

        if(iSysUserService.updateById(sysUser)){
            if(iSysUserRoleService.update(new SysUserRole(),
                    new UpdateWrapper<SysUserRole>().lambda()
                            .set(SysUserRole::getRoleId,update.getRoleId())
                            .eq(SysUserRole::getUserId,update.getId()))){
                return Response.success();
            }

        }

        return Response.error();
    }
}

