package com.huntmobi.springsecuritydemo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huntmobi.springsecuritydemo.base.http.Request;
import com.huntmobi.springsecuritydemo.base.http.Response;
import com.huntmobi.springsecuritydemo.base.pojo.SysRole;
import com.huntmobi.springsecuritydemo.base.pojo.SysRoleMenu;
import com.huntmobi.springsecuritydemo.base.dto.sysrole.Add;
import com.huntmobi.springsecuritydemo.base.dto.sysrole.FindByPage;
import com.huntmobi.springsecuritydemo.base.dto.sysrole.Oauth;
import com.huntmobi.springsecuritydemo.base.dto.sysrole.Update;
import com.huntmobi.springsecuritydemo.service.ISysRoleMenuService;
import com.huntmobi.springsecuritydemo.service.ISysRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private ISysRoleService iSysRoleService;

    @Autowired
    private ISysRoleMenuService iSysRoleMenuService;

    @PostMapping("/findByPage")
    public Request findByPage(@Valid @RequestBody FindByPage findByPage) {

        Page<SysRole> sysRolePage = new Page<>(findByPage.getPage(), findByPage.getLimit());

        //这里进行了模糊查询+分页查询条件
        IPage<SysRole> sysRoleIPage = iSysRoleService.page(sysRolePage, null);

        return Response.success(sysRoleIPage);
    }

    @PostMapping("/add")
    public Request add(@Valid @RequestBody Add add) {

        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(add, sysRole);

        if (iSysRoleService.save(sysRole)) {
            return Response.success();
        }

        return Response.error();
    }

    @PostMapping("/update")
    public Request update(@Valid @RequestBody Update update) {

        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(update, sysRole);
        if (iSysRoleService.updateById(sysRole)) {
            return Response.success();
        }

        return Response.error();
    }

    @PostMapping("/oauth")
    public Request oauth(@Valid @RequestBody Oauth oauth) {


        if (!oauth.getMenuIds().isEmpty()) {
            //删除原先的权限
            iSysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().lambda().eq(SysRoleMenu::getRoleId, oauth.getId()));

            List<SysRoleMenu> sysRoleMenus = new ArrayList<>();
            oauth.getMenuIds().forEach(menuId -> {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setRoleId(oauth.getId());
                sysRoleMenu.setMenuId(menuId);
                sysRoleMenus.add(sysRoleMenu);
            });
            iSysRoleMenuService.saveBatch(sysRoleMenus,20000);
            return Response.success();
        }


        return Response.error();
    }

    @PostMapping("/delete/{id}")
    public Request delete(@PathVariable String id) {
        if (iSysRoleService.remove(new QueryWrapper<SysRole>().lambda().eq(SysRole::getId, id))) {
            //删除角色关联表
            iSysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().lambda().eq(SysRoleMenu::getRoleId,id));
            return Response.success();
        }
        return Response.error();
    }

    @PostMapping("/selectList")
    public Request selectList() {
        return Response.success(iSysRoleService.list(new QueryWrapper<SysRole>().lambda().select(SysRole::getId, SysRole::getNameZh)));
    }

}

