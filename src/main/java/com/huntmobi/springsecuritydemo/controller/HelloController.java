package com.huntmobi.springsecuritydemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @BelongsProject: spring-security-demo
 * @BelongPackage: com.huntmobi.springsecuritydemo.controller
 * @Author: zouzhimin
 * @Date: 2020/11/19 9:11
 * @Description: TODO
 **/
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

}
