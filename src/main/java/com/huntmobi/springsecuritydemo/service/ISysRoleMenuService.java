package com.huntmobi.springsecuritydemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huntmobi.springsecuritydemo.base.pojo.SysRoleMenu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
