package com.huntmobi.springsecuritydemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huntmobi.springsecuritydemo.base.pojo.SysMenu;
import com.huntmobi.springsecuritydemo.base.dto.sysmenu.FindByPage;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface ISysMenuService extends IService<SysMenu> {
    List<FindByPage> findByPage(String id);
    List<FindByPage> leftSideMenu(String roleName);
}
