package com.huntmobi.springsecuritydemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huntmobi.springsecuritydemo.base.pojo.SysMenu;
import com.huntmobi.springsecuritydemo.base.dto.sysmenu.FindByPage;
import com.huntmobi.springsecuritydemo.mapper.SysMenuMapper;
import com.huntmobi.springsecuritydemo.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Override
    public List<FindByPage> findByPage(String id) {
        if(StringUtils.isEmpty(id)){
            return sysMenuMapper.findByPage(null);
        }
        return sysMenuMapper.findByPage(Long.parseLong(id));
    }

    @Override
    public List<FindByPage> leftSideMenu(String roleName) {
        return sysMenuMapper.leftSideMenu(roleName);
    }
}
