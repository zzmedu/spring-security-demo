package com.huntmobi.springsecuritydemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huntmobi.springsecuritydemo.base.pojo.SysRoleMenu;
import com.huntmobi.springsecuritydemo.mapper.SysRoleMenuMapper;
import com.huntmobi.springsecuritydemo.service.ISysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
