package com.huntmobi.springsecuritydemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huntmobi.springsecuritydemo.base.pojo.SysRole;
import com.huntmobi.springsecuritydemo.mapper.SysRoleMapper;
import com.huntmobi.springsecuritydemo.service.ISysRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
