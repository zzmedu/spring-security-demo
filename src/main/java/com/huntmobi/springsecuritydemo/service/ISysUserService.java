package com.huntmobi.springsecuritydemo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huntmobi.springsecuritydemo.base.pojo.SysUser;
import com.huntmobi.springsecuritydemo.base.dto.sysuser.FindByPage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface ISysUserService extends IService<SysUser> {

    IPage<FindByPage> findByPage(Page<SysUser> page);

}
